<?php
require 'vendor/autoload.php';
require 'Controller/smtp.php';

$website = 'https://mundodovinho.com.br';
$recipientMail = $_POST['email'];
$recipientName = 'Cadastro de e-mail';
$senderMail = 'contato@mundodovinho.com.br';
$senderName = 'Contato Mundo do Vinho';
$subject = '';

//Para nerds
$ipAddr = $_SERVER['REMOTE_ADDR'];
$browser = $_SERVER['HTTP_USER_AGENT'];
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

$message = '<h2>Novo cadastro de e-mail</h2><br><b>Endereço de e-mail: </b>' . $recipientMail
    . '<br><b>IP de Origem: </b>' . $ipAddr . '<br><b>Plataforma: </b>' . $browser . '<br><b>Data e horário do cadastro: </b>' . $date ;

$send = new \Controller\emailSend();

if ($send->send($senderMail, $senderName, $recipientMail, $recipientName, $subject, $message))
{
    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=' . $website . '">';
    echo '<script>function message() { alert("Cadastro realizado com sucesso!");}window.onload = message;</script>';

} else {

    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=' . $website . '">';
    echo '<script>function message() { alert("Falha ao realizar cadastro!");}window.onload = message;</script>';

}


