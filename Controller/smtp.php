<?php

namespace Controller;


$rootPath = $_SERVER['DOCUMENT_ROOT'];

require $rootPath . '/vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class emailSend
{

    protected $gmail, $global;

    public function __construct()
    {

        $conf = array(
            'char' => 'UTF-8',
            'host' => 'smtp.gmail.com',
            'SMTPSecure' => 'tls',
            'user' => 'email@gamail.com',
            'password' => 'senha',
            'port' => '587',
            'nickname' => 'Endereço de envio de e-mails'
        );

        $this->gmail =& $conf;

        global $rootPath;

        $global = array(
            'altBody' => 'Para melhor compatibilidade, utilize um cliente de e-mail com suporte à HTML.',
            'root' => $rootPath
        );

        $this->global =& $global;
    }

    public function send($senderMail, $senderName, $recipientMail, $recipientName, $subject, $message)
    {

            $msg = new PHPMailer();

            $msg->SetLanguage("br");
            $msg->IsHTML(true);


            $msg->CharSet = $this->gmail['char'];
            $msg->isSMTP();
            $msg->Host = $this->gmail['host'];
            $msg->SMTPAuth = true;
            $msg->SMTPSecure = $this->gmail['SMTPSecure'];
            $msg->Username = $this->gmail['user'];
            $msg->Password = $this->gmail['password'];
            $msg->Port = $this->gmail['port'];
            $msg->SMTPDebug = 0;

            $msg->setFrom($this->gmail['user'], $this->gmail['nickname']);

            $msg->addReplyTo($recipientMail, $recipientName);

            $msg->AddAddress($senderMail, $senderName);

            $msg->Subject = $subject;

            $msg->Body = $message;

            $msg->AltBody = $this->global['altBody'];

            if(!$msg->Send())
            {
                return false;

            } else {

                return true;
            }
    }
}